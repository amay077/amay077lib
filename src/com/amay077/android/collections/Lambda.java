package com.amay077.android.collections;

import hu.akarnokd.reactive4java.base.Action1;
import hu.akarnokd.reactive4java.base.Action2;
import hu.akarnokd.reactive4java.base.Func1;
import hu.akarnokd.reactive4java.base.Func2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.amay077.lang.Predicate1;

public class Lambda {

	private Lambda() { }

	public static <A> List<A> list(Iterator<? extends A> iterator) {
		List<A> list = new ArrayList<A>();
		if (iterator != null) {
			while (iterator.hasNext()) {
				list.add(iterator.next());
			}
		}
		return list;
	}

	@Deprecated
	public static <A> List<A> filter(
			Iterable<? extends A> it, 
			Func1<? super A, Boolean> f) {
		List<A> result = new ArrayList<A>();
		if (it != null) {
			for (A item : it) {
				if (f.invoke(item)) {
					result.add(item);
				}
			}
		}
		return result;
	}

	public static <A> List<A> filter(
			Iterable<? extends A> it, 
			Predicate1<? super A> f) {
		List<A> result = new ArrayList<A>();
		if (it != null) {
			for (A item : it) {
				if (f.invoke(item)) {
					result.add(item);
				}
			}
		}
		return result;
	}

	public static <A> List<A> filter(
			Iterator<? extends A> it, 
			Predicate1<? super A> f) {
		List<A> result = new ArrayList<A>();
		if (it != null) {
			while (it.hasNext()) {
				A item = (A)it.next();
				if (f.invoke(item)) {
					result.add(item);
				}
			}
		}
		return result;
	}
	
	public static <A, B> List<B> map(
			Iterable<? extends A> it, 
			Func1<? super A, B> f) {
		List<B> result = new ArrayList<B>();
		if (it != null) {
			for (A item : it) {
				result.add(f.invoke(item));
			}
		}
		return result;
	}

	public static <A, B> List<B> map(
			A[] arr, 
			Func1<? super A, B> f) {
		List<B> result = new ArrayList<B>();
		if (arr != null) {
			for (A item : arr) {
				result.add(f.invoke(item));
			}
		}
		return result;
	}

	public static <A, B> List<B> mapi(
			Iterable<? extends A> it, 
			Func2<Integer, ? super A, B> f) {
		List<B> result = new ArrayList<B>();
		
		Iterator<? extends A> iterator = it.iterator();
		int i = 0;
		if (it != null) {
			while (iterator.hasNext()) {
				result.add(f.invoke(i, iterator.next()));
				i++;
			}
		}
		return result;
	}
	
	public static <A, B> List<B> mapi(
			A[] arr, 
			Func2<Integer, ? super A, B> f) {
		List<B> result = new ArrayList<B>();
		
		for (int i = 0; i < arr.length; i++) {
			A a = arr[i];
			result.add(f.invoke(i, a));
		}
		
		return result;
	}
	
	@Deprecated
	public static <A> int findFirsti(
			Iterable<? extends A> it, 
			Func1<? super A, Boolean> f) {
		
		Iterator<? extends A> iterator = it.iterator();
		int i = 0;
		if (it != null) {
			while (iterator.hasNext()) {
				if (f.invoke(iterator.next())) {
					return i;
				}
				i++;
			}
		}

		return -1;
	}

	public static <A> int findFirsti(
			Iterable<? extends A> it, 
			Predicate1<? super A> f) {
		
		Iterator<? extends A> iterator = it.iterator();
		int i = 0;
		if (it != null) {
			while (iterator.hasNext()) {
				if (f.invoke(iterator.next())) {
					return i;
				}
				i++;
			}
		}

		return -1;
	}

	@Deprecated
	public static <A> A findFirst(
			Iterable<? extends A> it, 
			Func1<? super A, Boolean> f) {
		
		Iterator<? extends A> iterator = it.iterator();
		if (it != null) {
			while (iterator.hasNext()) {
				A item = iterator.next();
				if (f.invoke(item)) {
					return item;
				}
			}
		}
		return null;
	}

	public static <A> A findFirst(
			Iterable<? extends A> it, 
			Predicate1<? super A> f) {
		
		Iterator<? extends A> iterator = it.iterator();
		if (it != null) {
			while (iterator.hasNext()) {
				A item = iterator.next();
				if (f.invoke(item)) {
					return item;
				}
			}
		}
		return null;
	}

	@Deprecated
	public static <A> int findLast(
			List<? extends A> list, 
			Func1<? super A, Boolean> f) {
		
		if (list != null) {
			for (int i = list.size() - 1; i >= 0 ; i--) {
				if (f.invoke(list.get(i))) {
					return i;
				}
			}
		}		
		return -1;
	}

	public static <A> int findLast(
			List<? extends A> list, 
			Predicate1<? super A> f) {
		
		if (list != null) {
			for (int i = list.size() - 1; i >= 0 ; i--) {
				if (f.invoke(list.get(i))) {
					return i;
				}
			}
		}		
		return -1;
	}

	/**
	Functional 'fold' using an [Iterable]
	**/
	public static <A, B> B fold(Iterable<A> it, Func2<A, B, B> f, final B first) {
		B acc = first;
		
		if (it != null) {
			for (A item : it) {
				acc = f.invoke(item, acc);
			}
		}		
		return acc;
	}
	
	public static <A> void iter(Iterable<A> it, Action1<A> f) {
		if (it != null) {
			for (A item : it) {
				f.invoke(item);
			}
		}
	}

	public static <A> void iter(Iterator<A> it, Action1<A> f) {
		if (it != null) {
			while (it.hasNext()) {
				A item = (A)it.next();
				f.invoke(item);
			}
		}
	}

	public static <A> void iter(A[] array, Action1<A> f) {
		if (array != null) {
			for (A item : array) {
				f.invoke(item);
			}
		}
	}


	public static <A> void iteri(Iterable<A> it, Action2<Integer, A> f) {
		int i = 0;
		if (it != null) {
			for (A item : it) {
				f.invoke(i, item);
				i++;
			}
		}
	}

	public static <A> void iteri(Iterator<A> it, Action2<Integer, A> f) {
		int i = 0;
		if (it != null) {
			while (it.hasNext()) {
				A item = (A) it.next();
				f.invoke(i, item);
				i++;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <A> void iterr(Collection<A> coll, Action1<A> f) {
		if (coll.size() == 0) return;
		Object[] array = coll.toArray();
		if (array != null) {
			for (int i = array.length - 1; i >= 0; i--) {
				Object item = array[i];
				f.invoke((A)item);
			}
		}
	}
	
	@Deprecated
	public static <A> boolean exists(
			Iterable<? extends A> it, 
			Func1<? super A, Boolean> f) {
		if (it != null) {
			for (A item : it) {
				if (f.invoke(item)) {
					return true;
				}
			}
		}
		return false;
	}

	public static <A> boolean exists(
			Iterable<? extends A> it, 
			Predicate1<? super A> f) {
		if (it != null) {
			for (A item : it) {
				if (f.invoke(item)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static <A> List<A> reverse(List<A> list) {
		final List<A> result = new ArrayList<A>();
		
		Lambda.iterr(list, new Action1<A>() {
			@Override
			public void invoke(A item) {
				result.add(item);
			}
		});
		
		return result;
	}
}
