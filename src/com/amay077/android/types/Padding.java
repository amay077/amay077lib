package com.amay077.android.types;

public class Padding {
	private final int _value;
	
	public static Padding from(int value) {
		return new Padding(value);
	}
	
	private Padding(int value) {
		_value = value;
	}
	
	public int value() {
		return _value;
	}
}
