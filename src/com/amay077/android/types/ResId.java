package com.amay077.android.types;

public class ResId {
	static public final ResId NONE = new ResId(-1); 
	
	private final int _value;
	
	public static ResId from(int id) {
		return new ResId(id);
	}
	
	private ResId(int value) {
		_value = value;
	}
	
	public int value() {
		return _value;
	}
	
	public boolean	isNone() {
		return _value == ResId.NONE.value();
	}
}
