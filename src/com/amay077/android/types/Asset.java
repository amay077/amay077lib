package com.amay077.android.types;

public class Asset {
	static public final Asset NONE = new Asset(null); 
	
	private final String _value;
	
	public static Asset from(String name) {
		return new Asset(name);
	}
	
	private Asset(String value) {
		_value = value;
	}
	
	public String value() {
		return _value;
	}
	
	public boolean isNone() {
		return _value == Asset.NONE.value();
	}

}
