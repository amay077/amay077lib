package com.amay077.android.mvvm;

import hu.akarnokd.reactive4java.base.Action1;
import android.content.Intent;

public class SendImplicitIntentMessage implements Message {
	public final Intent intent;
	public final Action1<Intent> callback;

	public SendImplicitIntentMessage(Intent intent, Action1<Intent> callback) {
		this.intent = intent;
		this.callback = callback;
	}

	public boolean hasCallback() {
		return this.callback != null;
	}
}
