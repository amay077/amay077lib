package com.amay077.android.mvvm;

import com.amay077.android.types.ResId;

public abstract class ConfirmCommand extends DefaultCommand {
	private final Messenger _messenger;
	private final ResId _messageId;
	private final ResId _yesCaptionId;
	private final ResId _noCaptionId;

	public ConfirmCommand(Messenger messenger, 
			ResId messageId, ResId yesCaptionId, ResId noCaptionId) {
		_messenger = messenger;
		_messageId = messageId;
		_yesCaptionId = yesCaptionId;
		_noCaptionId = noCaptionId;
	}
	
	
	@Override
	public void execute() {
		if (canExecute()) {
			_messenger.send(new ConfirmDialogMessage(
					_messageId, _yesCaptionId, _noCaptionId, 
					new OnDialogResultListener<Boolean>() {
						@Override
						public void onDialogResult(Boolean result) {
							if (result) {
								onExecuteCommand();
							}
						}

						@Override
						public void onDismissDialog() {
						}
					}));
		}
	}
	

	protected abstract void onExecuteCommand();
}
