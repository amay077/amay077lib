package com.amay077.android.mvvm;

import hu.akarnokd.reactive4java.base.Action1;
import hu.akarnokd.reactive4java.base.Action2;
import hu.akarnokd.reactive4java.base.Func1;
import hu.akarnokd.reactive4java.base.Option;

import java.util.Comparator;
import java.util.List;

import com.amay077.android.collections.Lambda;
import com.amay077.android.util.Log;
import com.amay077.lang.IProperty;
import com.amay077.lang.IProperty.OnValueChangedListener;
import com.amay077.lang.Predicate2;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Binder extends BaseBinder {
	public enum BindingType { OneWay, TwoWay };
	
	private static final String TAG = "Binder";

	public Binder(Activity activity) {
		super(activity);
	}
	
	public <T> void toAction(IProperty<T> p, final Action1<T> uiUpdater) {
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				uiUpdater.invoke(newValue);
			}
		});
	}

	public void toCommand(final View v, final Command cmd) {

    	v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				executeCommand(cmd);
			}
		});
	}

	public <T> void toEnabled(
			final View v, 
			final IProperty<T> p,
			final Func1<T, Boolean> enabler) {
		
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				v.setEnabled(enabler.invoke(newValue));
			}
		});
	}
	
	public <T> void toVisibility(
			final View v, 
			final IProperty<T> p, 
			final Func1<T, Integer> visibler) {
		
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				v.setVisibility(visibler.invoke(newValue));
			}
		});
	}
	
	public <T> void toTextOneWay(final TextView tv, 
			final IProperty<T> p, final Func1<T, String> formatter) {
		
    	// oneWay
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				tv.setTextKeepState(formatter.invoke(newValue));
			}
		});
	}
	
	public void toText(final TextView tv, 
			final IProperty<String> p) {
		
    	// oneWay
		onChangedOnUiThread(p, new OnValueChangedListener<String>() {

			@Override
			public void onChanged(String newValue, String oldValue) {
				tv.setTextKeepState(newValue);

				if (tv instanceof EditText) {
					EditText editText = (EditText)tv;
					editText.setSelection(editText.getText().toString().length());
				}
			}
		});
    	
    	// twoWay
    	tv.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				p._setWithoutFire(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}
	
	public <T> void toTitle(final Activity activity, 
			final IProperty<T> p, final Func1<T, String> formatter) {
		
    	// oneWay
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				activity.setTitle(formatter.invoke(newValue));
			}
		});
	}
	
	public <A extends BaseAdapter, T> void toListAdapter(final A adapter, final IProperty<T> p, 
			final Action2<A, T> adapterUpdater) {
		
		onChangedOnUiThread(p, new OnValueChangedListener<T>() {
			@Override
			public void onChanged(T newValue, T oldValue) {
				adapterUpdater.invoke(adapter, newValue);
				adapter.notifyDataSetChanged();
			}
		});
	}

	public <T> void toListAdapter(final ArrayAdapter<T> adapter, final IProperty<Option<List<T>>> p) {
		
		toListAdapter(adapter, p, new Action2<ArrayAdapter<T>, Option<List<T>>>() {
			@Override
			public void invoke(final ArrayAdapter<T> a, Option<List<T>> items) {
				adapter.clear();
				if (Option.isSome(items)) {
					Lambda.iter(items.value(), new Action1<T>() {
						@Override
						public void invoke(T item) {
							a.add(item);
						}
					});
				}
			}
		});
	}

	public <A extends Adapter, T> void toListItemClick(final AdapterView<A> listView, final A adapter, 
			final SelectCommand<T> selectCommand) {
		
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				@SuppressWarnings("unchecked")
				T item = (T)listView.getItemAtPosition(position);
				executeSelectCommand(selectCommand, item);
			}
		});
	}

	public <A extends Adapter, T> void toListItemLongClick(final AdapterView<A> listView, final A adapter, 
			final SelectCommand<T> selectCommand) {
		
		listView.setAdapter(adapter);
		
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				@SuppressWarnings("unchecked")
				T item = (T)listView.getItemAtPosition(position);
				executeSelectCommand(selectCommand, item);
				return true;
			}
		});
	}

	public <A extends Adapter, T> void toListItemSelected(final AdapterView<A> listView, final Predicate2<T, T> c,
			final IProperty<Option<T>> p) {
		
		onChangedOnUiThread(p, new OnValueChangedListener<Option<T>>() {
			@Override
			public void onChanged(Option<T> newValue, Option<T> oldValue) {
				if (Option.isNone(newValue)) {
					listView.setSelected(false);
					return;
				}
				
				A adapter = listView.getAdapter();
				for (int i = 0; i < adapter.getCount(); i++) {
					Object obj = adapter.getItem(i);
					if (c.invoke(newValue.value(), (T)obj)) {
						listView.setSelection(i);
						break;
					}
				}
			}
		});
		
		listView.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View iew,
					int position, long id) {
				@SuppressWarnings("unchecked")
				T item = (T)listView.getItemAtPosition(position);
				p._setWithoutFire(item != null ? Option.some(item) : Option.<T>none());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				p._setWithoutFire(Option.<T>none());
			}
		});
	}


}
