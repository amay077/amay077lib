package com.amay077.android.mvvm;

public interface SelectCommand<T> {
	void execute(T item);
	boolean canExecute();
}
