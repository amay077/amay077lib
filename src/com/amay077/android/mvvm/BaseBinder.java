package com.amay077.android.mvvm;

import hu.akarnokd.reactive4java.base.Option;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.amay077.android.util.Log;
import com.amay077.lang.IProperty;
import com.amay077.lang.IProperty.OnValueChangedListener;
import com.amay077.lang.IProperty._OnValueChangedListener;

public abstract class BaseBinder {
	private static final String TAG = "BaseBinder";

	private WeakReference<Activity> _activity;

	@SuppressLint("UseSparseArrays")
	private final Map<Integer, Pair<IProperty<?>, OnValueChangedListener<?>>> _handlerMap = 
			new HashMap<Integer, Pair<IProperty<?>, OnValueChangedListener<?>>>();

	public BaseBinder(Activity activity) {
		_activity =  new WeakReference<Activity>(activity);
	}
	
	protected Activity getActivity() {
		return _activity.get();
	}
	
	protected void executeCommand(Command command) {
		if (!command.canExecute()) {
			Log.d(TAG, "command is not exectable - " + command.getClass().getName());
			return;
		}
		command.execute();
	}
	
	protected <T> void executeSelectCommand(SelectCommand<T> command, T arg) {
		if (!command.canExecute()) {
			Log.d(TAG, "command is not exectable - " + command.getClass().getName());
			return;
		}
		
		if (arg != null) {
			command.execute(arg);
		}
	}

	protected <T> void onChangedOnUiThread(IProperty<T> p, final OnValueChangedListener<T> changeHandler) {
		OnValueChangedListener<T> listener = new _OnValueChangedListener<T>() {
			@Override
			public void onChanged(final T newValue, final T oldValue) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						changeHandler.onChanged(newValue, oldValue);
					}
				});
			}
		};
		
		_handlerMap.put(changeHandler.hashCode(), new Pair<IProperty<?>, OnValueChangedListener<?>>(p, listener));
		p.addListener(listener);
	}
	
	protected void runOnUiThread(Runnable runnnable) {
		_activity.get().runOnUiThread(runnnable);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> void unregisterHandler(OnValueChangedListener<T> handler) {
		Pair<IProperty<?>, OnValueChangedListener<?>> item = _handlerMap.get(handler.hashCode());
		
		IProperty<T> first = (IProperty<T>)item.first;
		OnValueChangedListener<T> second = (OnValueChangedListener<T>) item.second;
		
		first.removeListener(second);
	}

	public void toToast(View v, final String toastMessage) {
    	v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				_activity.get().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(_activity.get(),
								toastMessage, Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
	}

}
