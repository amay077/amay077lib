package com.amay077.android.mvvm;

import android.app.Application;
import android.util.SparseArray;

public class BaseApplication extends Application implements CallbackStore {
    /**
     * 遷移先 Activity からの戻り時の Action を管理するマップ
     */
	private SparseArray<OnActivityResultCallback> _activityCallbacks =
		new SparseArray<OnActivityResultCallback>();

	
	/**
	 * 保持されている callback の数を取得する
	 */
	@Override
	public int getActivityCallbackNum() {
		return _activityCallbacks.size();
	}
	
	/**
	 * callback を登録する 
	 */
	@Override
	public void putActivityCallback(int requestCode, OnActivityResultCallback resultAction) {
		_activityCallbacks.put(requestCode, resultAction);
	}

	/**
	 * requestCode に対応する callback を返す。見つからない場合は null を返す。 
	 */
	@Override
	public OnActivityResultCallback getActivityCallback(int requestCode) {
		if (_activityCallbacks.indexOfKey(requestCode) >= 0) {
			OnActivityResultCallback callback = _activityCallbacks.get(requestCode);
			_activityCallbacks.delete(requestCode);
			return callback;
		} else {
			return null;
		}
	}

}
