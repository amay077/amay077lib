package com.amay077.android.mvvm;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public final class Messenger {
	public interface OnReceiveListener<T extends Message> {
		void onReceive(T message);
	}
	
	private Map<String, OnReceiveListener<? extends Message>> _listeners = 
			new HashMap<String, OnReceiveListener<? extends Message>>();
	
	@SuppressWarnings("unchecked")
	public void send(Message message) {
		final String messengerTypeName = message.getClass().getName();
		
		if (!_listeners.containsKey(messengerTypeName)) {
			return;
		}
		
		@SuppressWarnings("rawtypes")
		OnReceiveListener action1 = _listeners.get(messengerTypeName);
		action1.onReceive(message);
	}

	public <T extends Message> void register(OnReceiveListener<T> listener) {
		Type[] types = listener.getClass().getGenericInterfaces();
		String typeString = types[0].toString();

		int start = typeString.indexOf("<");
		int end = typeString.lastIndexOf(">");
		
		String nameOfT = typeString.subSequence(start + 1, end).toString();
		
		_listeners.put(nameOfT, listener);
	}
}
