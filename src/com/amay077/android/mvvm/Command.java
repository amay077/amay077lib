package com.amay077.android.mvvm;

public interface Command {
	void execute();
	boolean canExecute();
}
