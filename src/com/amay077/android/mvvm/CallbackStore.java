package com.amay077.android.mvvm;

import android.content.Intent;
import android.os.Parcelable;

public interface CallbackStore {
	public interface OnActivityResultCallback {
		/**
		 * Activity からの戻り時に呼び出されるコールバック。
		 * 基本 onActivityresult のラッパだが、第３引数は Intent でなくパラメータそのもの。
		 */
		void onResult(int requestCode, int resultCode, Parcelable resultData, Intent intent);
	}

	int getActivityCallbackNum();
	
	void putActivityCallback(int requestCode, OnActivityResultCallback callback);
	
	OnActivityResultCallback getActivityCallback(int requestCode);
}
