package com.amay077.android.mvvm;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import hu.akarnokd.reactive4java.base.Action1;
import hu.akarnokd.reactive4java.base.Option;
import hu.akarnokd.reactive4java.reactive.Observable;
import hu.akarnokd.reactive4java.reactive.Observer;

import com.amay077.android.collections.Lambda;
import com.amay077.android.util.Log;
import com.amay077.lang.ObservableValue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;


public abstract class BaseViewModel {
	
	public interface ObserverUnregistrar {
		int getId();
	}
	
	class ObserverUnregistrarImpl implements ObserverUnregistrar {
		private int _id;
		private ObserverUnregistrarImpl(int id) {
			_id = id;
		}
		
		@Override
		public int getId() {
			return _id;
		}
	}

	private static final String TAG = "BaseViewModel";
	public final ObservableValue<String> toastMessage = new ObservableValue<String>();
	public final ObservableValue<Integer> toastMessageId = new ObservableValue<Integer>();
	public final ObservableValue<Integer> indicatorMessegeId = new ObservableValue<Integer>();
	public final ObservableValue<Boolean> visibleIndicator = new ObservableValue<Boolean>();
	
	private final Messenger _messenger = new Messenger();
	@SuppressLint("UseSparseArrays")
	private Map<Integer, Closeable> _closerMap = new HashMap<Integer, Closeable>();
	/* package */ Context _appContext;
	
	protected void onIntent(Intent intent) { };
	protected abstract void onBindViewCompleted(Option<Parcelable> startupParam);
	protected void onPauseView() { };
	protected void onResumeView() { };
	protected void onDestroyView() {
		unregisterAllObservers();
	}

	protected BaseViewModel() {
	}
	
	public Messenger getMessenger() {
		return _messenger ;
	}
	
	public Context getAppContext() {
		return _appContext;
	}
	
	protected void startViewModel(Class<? extends Activity> activityClass, Parcelable value) {
		_messenger.send(new StartActivityMessage(activityClass, value));
	}
	
	protected void startViewModel(Class<? extends Activity> activityClass) {
		_messenger.send(new StartActivityMessage(activityClass));
	}
	
	public <T> ObserverUnregistrar registerObserver(Observable<T> observable, Observer<T> observer) {
		Closeable closer = observable.register(observer);
		_closerMap.put(closer.hashCode(), closer);
		return new ObserverUnregistrarImpl(closer.hashCode());
	}
	
	public void unregisterObserver(ObserverUnregistrar unregistrar) {
		if (unregistrar == null) {
			return;
		}
		
		if (!_closerMap.containsKey(unregistrar.getId())) {
			return;
		}
		
		Closeable closer = _closerMap.get(unregistrar.getId());
		_closerMap.remove(unregistrar.getId());
		_forceClose(closer);
		
	}
	
	public void unregisterAllObservers() {
		Lambda.iterr(_closerMap.values(), new Action1<Closeable>() {
			@Override
			public void invoke(Closeable closer) {
				_forceClose(closer);
			}
		});
		
		_closerMap.clear();
	}
	
	private static void _forceClose(Closeable closer) {
		if (closer != null) {
			try {
				closer.close();
			} catch (IOException e) {
				Log.w(TAG, "_forceClose close failed.", e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends Parcelable> Option<T> parseStartupParam(Option<Parcelable> startupParam) {
		if (Option.isSome(startupParam)) {
			try {
				return Option.some((T)startupParam.value());
			} catch (Exception e) {
				Log.w(TAG, "startupParam cast failed.", e);
				return Option.none();
			}
		} else {
			return Option.none();
		}
	}
	
}
