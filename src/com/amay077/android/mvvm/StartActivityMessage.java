package com.amay077.android.mvvm;

import hu.akarnokd.reactive4java.base.Action1;
import hu.akarnokd.reactive4java.base.Option;
import android.os.Parcelable;

public class StartActivityMessage implements Message {

	public final Class<?> activityClass;
	private Option<Parcelable> _parcel = Option.none();
	private Option<Action1<Parcelable>> _callback = Option.none();
	public final String action;
	
	public StartActivityMessage(Class<?> activityClass) {
		this.activityClass = activityClass;
		action = null;
	}

	public StartActivityMessage(Class<?> activityClass,
			Parcelable parcelable) {
		
		this.activityClass = activityClass;
		_parcel = Option.some(parcelable);
		action = null;
	}

	public StartActivityMessage(Class<?> activityClass,
			Parcelable parcelable, Action1<Parcelable> callback) {
		
		this.activityClass = activityClass;
		_parcel = Option.some(parcelable);
		action = null;
		if (callback != null) {
			_callback = Option.some(callback);
		}
	}
	
	public StartActivityMessage(String action) {
		this.activityClass = null;
		this.action = action;
	}
	
	public boolean hasParcel() {
		return Option.isSome(_parcel);
	}

	public Parcelable getParcel() {
		return _parcel.value();
	}
	
	public boolean hasCallback() {
		return Option.isSome(_callback);
	}

	public Action1<Parcelable> getCallback() {
		return _callback.value();
	}
	
	public boolean hasAction() {
		return action != null && action != "";
	}
}
