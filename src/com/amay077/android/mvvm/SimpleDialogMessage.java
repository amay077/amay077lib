package com.amay077.android.mvvm;

import com.amay077.android.types.ResId;

public class SimpleDialogMessage<TResult> implements Message {
	public final ResId messageId;
	public final OnDialogResultListener<TResult> dialogResultListener; 
	
	public SimpleDialogMessage(ResId messageId, OnDialogResultListener<TResult> dialogResultListener) {
		this.messageId = messageId;
		this.dialogResultListener = dialogResultListener;
	}
}
