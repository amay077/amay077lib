package com.amay077.android.mvvm;

public interface OnDialogResultListener<TResult> {
	void onDialogResult(TResult result);
	void onDismissDialog();
}

