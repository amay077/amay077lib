package com.amay077.android.mvvm;

import hu.akarnokd.reactive4java.base.Option;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import com.amay077.android.mvvm.CallbackStore.OnActivityResultCallback;
import com.amay077.android.mvvm.Messenger.OnReceiveListener;
import com.amay077.android.util.Log;
import com.amay077.lang.IProperty.OnValueChangedListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * ベースアクティビティクラス
 *
 */
public abstract class BaseActivityAdapter<T extends BaseViewModel> {
	private static final String TAG = "BaseActivityAdapter";

	private static final int DIALOG_ID_PROGRESS = 999;

	private final WeakReference<Activity> _activity;
	
	/**
	 * 表示する Dialog を管理するマップ。
	 */
	private SparseArray<Dialog> _dialogMap = new SparseArray<Dialog>();

	@SuppressLint("UseSparseArrays")
	private Map<Integer, Command> _menuMap = new HashMap<Integer, Command>();

	private T _viewModel;
	private final Class<T> _classVM;

	public BaseActivityAdapter(Activity activity, Class<T> classVm) {
		_activity = new WeakReference<Activity>(activity);
		_classVM = classVm;
	}

    /**
     * このアプリの Application クラスを取得する 
     */
	public CallbackStore getApp() {
    	return (CallbackStore)_activity.get().getApplication();
    }

    /**
     * 戻り時の Action を指定して、画面遷移する 
     */
	public void startActivityWithResultAction(Context packageContext, Class<?> cls,
			OnActivityResultCallback resultAction) {
		CallbackStore app = getApp();
		Integer requestCode = app.getActivityCallbackNum();
		app.putActivityCallback(requestCode, resultAction);

		Intent intent = new Intent(packageContext, cls);
		_activity.get().startActivityForResult(intent, requestCode);
	}
	
    /**
     * パラメータを指定して画面遷移する 
     */
	public void startActivityWithParam(Context packageContext, Class<?> cls, 
			Parcelable param) {
		Intent intent = new Intent(packageContext, cls);
		intent.putExtra("startup_param", param);
		
		_activity.get().startActivity(intent);
	}

    /**
     * パラメータと、戻り時の Action を指定して、画面遷移する 
     */
	public void startActivityWithParamAndResultAction(Context packageContext, Class<?> cls,
			Parcelable param, OnActivityResultCallback resultAction) {
		Intent intent = new Intent(packageContext, cls);
		if (param != null) {
			intent.putExtra("startup_param", param);
		}
		
		startActivityWithIntentAndResultAction(intent, resultAction);
	}

    /**
     * Intent と、戻り時の Action を指定して、画面遷移する 
     */
	protected final void startActivityWithIntentAndResultAction(Intent intent, 
			OnActivityResultCallback resultAction) {
		CallbackStore app = getApp();
		Integer requestCode = app.getActivityCallbackNum();
		app.putActivityCallback(requestCode, resultAction);

		_activity.get().startActivityForResult(intent, requestCode);
	}

	/**
	 * 呼び出し時のパラメータを取得する
	 */
	private Option<Parcelable> getStartupParameter() {
		Intent intent = _activity.get().getIntent();
		if (intent == null) {
			return Option.none();
		}
		
		if (!intent.hasExtra("startup_param")) {
			return Option.none();
		}
		
		return Option.some(intent.getParcelableExtra("startup_param")); 
	}
	
	/**
	 * 戻り時のパラメータを設定する。
	 */
	public void setOkResult(Parcelable result) {
		Intent resultIntent = new Intent();
		resultIntent.putExtra("activity_result", result);
		_activity.get().setResult(Activity.RESULT_OK, resultIntent);
	}

	public boolean hasFormatableActivityResult(int requestCode, int resultCode, Intent data) {
		CallbackStore app = getApp();
		OnActivityResultCallback callback = app.getActivityCallback(requestCode); 
		return resultCode == Activity.RESULT_OK && callback != null;
	}
	
	/**
	 * Activity からの戻り時に、startActivityWithResultAction で渡された Action を呼び出す。
	 * 
	 * startActivityWithResultAction を実行した際、渡された Action をマップに追加します(Key は連番=requestCode)。
	 * その requestCode から Action を特定し、Invoke します。
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		CallbackStore app = getApp();
		OnActivityResultCallback callback = app.getActivityCallback(requestCode); 
		if (resultCode == Activity.RESULT_OK && callback != null) {
			Parcelable resultData = data.getParcelableExtra("activity_result");
			callback.onResult(requestCode, resultCode, resultData, data);
		}
	}

	/**
	 * Dialog インスタンスを直接指定してダイアログを表示する。
	 * 
	 * Android のライフサイクルでは、showDialog(id) ⇢ onCreateDialog(id) 
	 * ⇢ dissmissDialog(id) の流れに沿わなければならないが、
	 * この方法では「ダイアログを表示させる処理の付近に、そのコールバックを書く」事ができず、
	 * 処理が分散してしまい見づらい。
	 * showDialogWithDialog(dialog) は直接 Dialog のインスタンスを指定して呼び出せる。
	 * 内部では、渡された dialog を hashCode をキーにして管理し、onCreateDialog にて呼び出している。
	 * 但し、setOnDismissListener は、このメソッドにより上書きされる。
	 */
	public void showDialogWithDialog(AlertDialog dialog, final DialogMessage2 message) {
		int dialogId = dialog.hashCode();
		_dialogMap.put(dialogId, dialog);
		
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				if (message != null) {
					message.onDismissDialog();
				}
				removeDialog(dialog);
			}
		});
		
		_activity.get().showDialog(dialogId);
	}
	
	/**
	 * Dialog が生成される時
	 * 
	 * id が _dialogMap で管理されている値(hashcode)だったら、それを返す。
	 */
	public Dialog onCreateDialog(int id) {
		if (id == DIALOG_ID_PROGRESS) {
			return createProgressDialog();
		} else if (_dialogMap.indexOfKey(id) <= 0) {
			return _dialogMap.get(id);
		}
		
		return null;
	}
	
	public void onPrepareDialog(int id, Dialog dialog) {
		if (id == DIALOG_ID_PROGRESS && _viewModel != null) {
			ProgressDialog plg = (ProgressDialog)dialog;
			plg.setMessage(_activity.get().getString(_viewModel.indicatorMessegeId.get()));
		}
	}
	
	/**
	 * Dialog を破棄し、キャッシュから削除する。
	 * 
	 * showDialogWithDialog(dlg) で表示させたダイアログは、必ずこのメソッドで破棄する必要がある。
	 */
	private void removeDialog(DialogInterface dialog) {
		int id = dialog.hashCode();
		if (_dialogMap.indexOfKey(id) <= 0) {
			_activity.get().removeDialog(id);
			_dialogMap.remove(id);
		}
	}

	
	private Dialog createProgressDialog() {
		ProgressDialog dialog = new ProgressDialog(_activity.get());
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setIndeterminate(true);
		return dialog;
	}
	
    @SuppressWarnings("unchecked")
	protected void bindViewModel() {
    	if (_viewModel != null) {
    		return;
    	}
    	
    	Constructor<? extends BaseViewModel> ctor;
		Log.d(TAG, "bindViewModel called."); 
		try {
			ctor = _classVM.getConstructor();
	    	_viewModel = (T)ctor.newInstance();
	    	_viewModel._appContext = _activity.get().getApplicationContext();
	    	onBindViewModel(_viewModel);
	    	onBindViewMenu(_viewModel, _menuMap);

	    	// 既定の Messenger を登録
	    	registerDefaultMessages();
	    	
	    	// 既定の Observable をバインド。
	    	bindDefaultObservables();
	    	
	    	
	    	if (_activity.get() != null && _activity.get().getIntent() != null) {
	    		_viewModel.onIntent(_activity.get().getIntent());
	    	}
	    	
	    	Option<Parcelable> startupParcel = getStartupParameter();
	    	_viewModel.onBindViewCompleted(startupParcel);
		} catch (SecurityException e) {
			Log.e(TAG, "bindViewModel security failed.", e); 
		} catch (NoSuchMethodException e) {
			Log.e(TAG, "bindViewModel nosuch method failed.", e); 
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "bindViewModel illegal argument failed.", e); 
		} catch (InstantiationException e) {
			Log.e(TAG, "bindViewModel instantiation failed.", e); 
		} catch (IllegalAccessException e) {
			Log.e(TAG, "bindViewModel illegal access failed.", e); 
		} catch (InvocationTargetException e) {
			Log.e(TAG, "bindViewModel invocation target failed.", e); 
		}
	}
    
    private void bindDefaultObservables() {
    	// プログレスダイアログ
    	_viewModel.visibleIndicator.addListener(new OnValueChangedListener<Boolean>() {
			@Override
			public void onChanged(final Boolean newValue, final Boolean oldValue) {
				_activity.get().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							if (newValue) {
								_activity.get().showDialog(DIALOG_ID_PROGRESS);
							} else {
								_activity.get().dismissDialog(DIALOG_ID_PROGRESS);
							}
						} catch (Exception e) {
							String err = "ProgressDialog " + (newValue ? "show" : "dismiss") + " failed.";
							Log.e(TAG, err, e);
							Toast.makeText(_activity.get(), err, Toast.LENGTH_SHORT).show();
						}
					}
				});
			}
		});
    	
    	// Toast
    	_viewModel.toastMessageId.addListener(new OnValueChangedListener<Integer>() {
			private Toast _toast;

			@Override
			public void onChanged(final Integer newValue, Integer oldValue) {
				if (newValue == null) {
					return;
				}
				
				_activity.get().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (_toast != null) {
							_toast.cancel();
						}
						_toast = Toast.makeText(_activity.get(), _activity.get().getString(newValue), Toast.LENGTH_SHORT);
						_toast.show();
						_viewModel.toastMessage.set(""); // すぐにクリア。
					}
				});
			}
		});
    	
    	_viewModel.toastMessage.addListener(new OnValueChangedListener<String>() {
			private Toast _toast;

			@Override
			public void onChanged(final String newValue, final String oldValue) {
				if (newValue == null || newValue.equals("")) {
					return;
				}
				
				_activity.get().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (_toast != null) {
							_toast.cancel();
						}
						_toast = Toast.makeText(_activity.get(), newValue, Toast.LENGTH_SHORT);
						_toast.show();
						_viewModel.toastMessage.set(""); // すぐにクリア。
					}
				});
			}
		});
	}

	private void registerDefaultMessages() {
    	Messenger messenger = _viewModel.getMessenger();
    	
		// 画面遷移
		messenger.register(new OnReceiveListener<StartActivityMessage>() {
			@Override
			public void onReceive(final StartActivityMessage arg) {
				
				if (arg.hasAction()) {
					_activity.get().startActivity(new Intent(arg.action));
					return;
				}
				
				Parcelable parcel = null;
				if (arg.hasParcel()) {
					parcel = arg.getParcel();
				}
				
				startActivityWithParamAndResultAction(_activity.get(), 
						arg.activityClass, parcel, new OnActivityResultCallback() {
					@Override
					public void onResult(int requestCode, int resultCode, Parcelable resultData, Intent intent) {
						if (arg.hasCallback()) {
							arg.getCallback().invoke(resultData);
						}
					}
				});
			}
		});
		
		// 暗黙的Intent の送信
		messenger.register(new OnReceiveListener<SendImplicitIntentMessage>() {
			@Override
			public void onReceive(final SendImplicitIntentMessage message) {
				startActivityWithIntentAndResultAction(message.intent, new OnActivityResultCallback() {
					@Override
					public void onResult(int requestCode, int resultCode, Parcelable resultData, Intent intent) {
						if (message.hasCallback()) {
							message.callback.invoke(intent);
						}
					}
				});
			}
		});
    	
    	// Activity 終了メッセージ
    	messenger.register(new OnReceiveListener<FinishActivityMessage>() {
			@Override
			public void onReceive(FinishActivityMessage m) {
				Activity activity = _activity.get();
				if (m.getOkResult() != null) {
					setOkResult(m.getOkResult());
				}
				activity.finish();
			}
		});
    	
    	// Dialog メッセージ
    	messenger.register(new OnReceiveListener<ConfirmDialogMessage>() {
			@Override
			public void onReceive(final ConfirmDialogMessage m) {
				AlertDialog dialog = createConfirmDialog(
						_activity.get().getString(m.messageId.value()), 
						_activity.get().getString(m.yesCaptionId.value()), 
						_activity.get().getString(m.noCaptionId.value()), m.dialogResultListener);
				showDialogWithDialog(dialog, null);
			}
		});
    	
    	// Dialog2 メッセージ
    	messenger.register(new OnReceiveListener<DialogMessage2>() {
			@Override
			public void onReceive(final DialogMessage2 m) {
				AlertDialog dialog = m.createDialog(_activity.get());
				showDialogWithDialog(dialog, m);
			}
		});
	}
	
    protected void bindMenu(Map<Integer, Command> menuMap) {
    	_menuMap = menuMap;
    }

	protected AlertDialog createConfirmDialog(final String message, final String yesCaption, final String noCaption, 
    		final OnDialogResultListener<Boolean> listener) {
    	return new AlertDialog.Builder(_activity.get())
		.setMessage(message)
		.setPositiveButton(yesCaption, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.onDialogResult(true);
			}
		})
		.setNegativeButton(noCaption, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.onDialogResult(false);
			}
		})
		.create();
	}

    /** Command を実行する共通関数 */
	protected void executeCommand(Command command) {
		if (!command.canExecute()) {
			Log.d(TAG, "command is not exectable - " + command.getClass().getName());
			return;
		}
		command.execute();
	}

	protected void setKeepScreenOn() {
		_activity.get().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);	
	}
	protected void setKeepScreenOff() {
		_activity.get().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

    // TODO abstract にしたいが、既存 Activity を修正するのが面倒なのであとで直す
    protected abstract void onBindViewModel(T vm);
    protected abstract void onBindViewMenu(T vm, Map<Integer, Command> menuMap);
    
    protected void onPause() {
    	_viewModel.onPauseView();
    }
    protected void onResume() {
    	_viewModel.onResumeView();
    }
    
    protected void onDestroy() {
    	if (_viewModel != null) {
    		_viewModel.onDestroyView();
    	}
    }
    
    public boolean isBindedMenu(MenuItem item) {
    	return _menuMap.containsKey(item.getItemId());
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	if (isBindedMenu(item)) {
    		Command command = _menuMap.get(item.getItemId());
    		if (command.canExecute()) {
    			command.execute();
    			return false;
    		} else {
    			return false;
    		}
    	} else {
    		return false;
    	}
    }

	public T getViewModel() {
		return _viewModel;
	}
}
