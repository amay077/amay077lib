package com.amay077.android.mvvm;

import com.amay077.android.types.ResId;

public class ConfirmDialogMessage extends SimpleDialogMessage<Boolean> {
	public final ResId yesCaptionId;
	public final ResId noCaptionId;
	
	public ConfirmDialogMessage(
			ResId messageId, ResId yesCaptionId, ResId noCaptionId,
			OnDialogResultListener<Boolean> dialogResultListener) {
		super(messageId, dialogResultListener);
		
		this.yesCaptionId = yesCaptionId;
		this.noCaptionId = noCaptionId;
	}
}
