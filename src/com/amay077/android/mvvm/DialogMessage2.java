package com.amay077.android.mvvm;

import java.util.List;

import com.amay077.android.types.ResId;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class DialogMessage2 implements Message {
	public static class Buttons {
		public ResId yesCaptionId;
		public ResId noCaptionId;
		
		public Buttons(ResId yesCaptionId) {
			this(yesCaptionId, ResId.NONE);
		}

		public Buttons(ResId yesCaptionId, ResId noCaptionId) {
			this.yesCaptionId = yesCaptionId;
			this.noCaptionId = noCaptionId;
		}
	}

	public final ResId _titleId;
	public final ResId _messageId;
	public final ResId _yesCaptionId;
	public final ResId _noCaptionId;
	public final OnDialogResultListener<Integer> _dialogResultListener;
	private final List<String> _items; 
	
	public DialogMessage2(
			ResId titleId, 
			ResId messageId, 
			OnDialogResultListener<Integer> dialogResultListener) {
		this(titleId, messageId, new Buttons(ResId.from(android.R.string.ok)), dialogResultListener);
	}

	public DialogMessage2(ResId messageId) {
		this(ResId.NONE, messageId, new Buttons(ResId.from(android.R.string.ok)), null);
	}

	public DialogMessage2(
			ResId messageId, 
			Buttons buttons,
			OnDialogResultListener<Integer> dialogResultListener) {
		this(ResId.NONE, messageId, buttons, dialogResultListener);
	}

	public DialogMessage2(
			ResId titleId, 
			ResId messageId, 
			Buttons buttons,
			List<String> items,
			OnDialogResultListener<Integer> dialogResultListener) {
		_titleId = titleId;
		_messageId = messageId;
		_dialogResultListener = dialogResultListener;
		_items = items;
		if (buttons != null) {
			_yesCaptionId = buttons.yesCaptionId;
			_noCaptionId = buttons.noCaptionId;
		} else {
			_yesCaptionId = ResId.NONE;
			_noCaptionId = ResId.NONE;
		}
	}
	
	public DialogMessage2(
			ResId titleId, 
			ResId messageId, 
			Buttons buttons,
			OnDialogResultListener<Integer> dialogResultListener) {
		this(titleId, messageId, buttons, null, dialogResultListener);
	}

	public DialogMessage2(
			ResId titleId, 
			List<String> items,
			OnDialogResultListener<Integer> dialogResultListener) {
		this(titleId, null, (Buttons)null, items, dialogResultListener);
	}
	
	AlertDialog createDialog(Context context) {
		
		final OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (_dialogResultListener != null) {
					_dialogResultListener.onDialogResult(which);
				}
			}
		};
		
		Builder builder = new AlertDialog.Builder(context);
		
		if (hasItems()) {
			String[] items = _items.toArray(new String[0]);
			builder = builder.setItems(items, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					_dialogResultListener.onDialogResult(which);
				}
			});
		} else {
			builder = builder.setMessage(_messageId.value());
		}
		
		AlertDialog dlg = builder.create();
		dlg.setCanceledOnTouchOutside(false);
		if (!_titleId.isNone()) dlg.setTitle(_titleId.value());

		if (!hasItems()) {
			if (!_yesCaptionId.isNone()) {
				dlg.setButton(context.getString(_yesCaptionId.value()), clickListener);
			}
			if (!_noCaptionId.isNone()) {
				dlg.setButton2(context.getString(_noCaptionId.value()), clickListener);
			}
		}

		
		return dlg;
	}
	
	private boolean hasItems() {
		return _items != null && _items.size() > 0;
	}

	/* package */ void onDismissDialog() {
		if (_dialogResultListener != null) {
			_dialogResultListener.onDismissDialog();
		}
	}

}
