package com.amay077.lang;

public class ObjectUtil {
	public static boolean equals(Object a, Object b) {
		if (a == null) {
			return b == null;
		} else {
			return a.equals(b);
		}
	}
}
