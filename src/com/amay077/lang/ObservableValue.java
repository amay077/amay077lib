package com.amay077.lang;


public class ObservableValue<T> extends Property<T> {
	private T _value;
	
	public ObservableValue() {
	}
	public ObservableValue(T defaultValue) {
		_value = defaultValue;
	}

	@Override
	public T get() {
		return _value;
	}

	@Override
	public void set(T value) {
		if (!ObjectUtil.equals(_value, value)) {
			T oldValue = _value;
			_value = value;
			fireOnChanged(oldValue, value);
		}
	}

	@Override
	public void _setWithoutFire(T value) {
		if (!ObjectUtil.equals(_value, value)) {
			T oldValue = _value;
			_value = value;
			_fireOnChanged(oldValue, value);
		}
	}
	
	@Override
	public void setDefault(T defaultValue) {
		_value = defaultValue;
		super.setDefault(defaultValue);
	}
}
