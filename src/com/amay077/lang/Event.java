package com.amay077.lang;

import java.util.ArrayList;
import java.util.List;

public class Event<T> {
	public interface EventHandler<T> {
		void handle(Object sender, T data);
	}
	
	private final List<EventHandler<T>> _actions = new ArrayList<EventHandler<T>>();
	
	public void add(EventHandler<T> action) {
		_actions.add(action);
	}

	public void remove(EventHandler<T> action) {
		_actions.remove(action);
	}
	
	public void clear() {
		_actions.clear();
	}

	public void fire(Object sender, T data) {
		for (EventHandler<T> act : _actions) {
			act.handle(sender, data);
		}
	}
}
