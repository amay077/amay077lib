package com.amay077.lang;

public interface Predicate2<T1, T2> {
	boolean invoke(T1 arg0, T2 arg1);
}
