package com.amay077.lang;

public class StringUtil {
	static public String[] toArray(String buf, String splitter) {
		if (StringUtil.isNullOrEmpty(buf)){
        	return null;
		}

		if (StringUtil.isNullOrEmpty(splitter)) {
        	return null;
		}

		return buf.split(splitter);
	}

	static public String fromArray(String[] array, String splitter) {
		StringBuilder builder = new StringBuilder();
		boolean theFirst = true;
		for (String string : array) {
			if (!theFirst) {
				builder.append(splitter);
			} else {
				theFirst = false;
			}
			builder.append(string);
		}

		return builder.toString();
	}

	static public boolean isNullOrEmpty(String value) {
		return (value == null) || (value == "");
	}
}
