package com.amay077.lang;

public interface IProperty<T> {
	interface OnValueChangedListener<T> {
		void onChanged(T newValue, T oldValue);
	}
	
	interface _OnValueChangedListener<T> extends OnValueChangedListener<T> {
	}
	
	public T get();
	public void set(T value);
	public void _setWithoutFire(T value);
	
	void setDefault(T defaultValue);
	
	void addListener(OnValueChangedListener<T> l);
	void removeListener(OnValueChangedListener<T> l);
	void clearListeners();
}
