package com.amay077.lang;

import java.util.ArrayList;
import java.util.List;


public abstract class Property<T> implements IProperty<T> {
	private List<OnValueChangedListener<T>> _listeners = 
			new ArrayList<OnValueChangedListener<T>>();

	@Override
	public void setDefault(T defaultValue) {
		fireOnChanged(null, defaultValue);
	}

	protected final void fireOnChanged(T oldValue, T newValue) {
		for (OnValueChangedListener<T> l : _listeners) {
			l.onChanged(newValue, oldValue);
		}
	}

	protected final void _fireOnChanged(T oldValue, T newValue) {
		for (OnValueChangedListener<T> l : _listeners) {
			if (l instanceof _OnValueChangedListener) {
				continue;
			}
			
			l.onChanged(newValue, oldValue);
		}
	}
	
	public final void addListener(OnValueChangedListener<T> l) {
		_listeners.add(l);
	}
	
	public final void removeListener(OnValueChangedListener<T> l) {
		_listeners.remove(l);
	}
	
	public final void clearListeners() {
		_listeners.clear();
	}
}
