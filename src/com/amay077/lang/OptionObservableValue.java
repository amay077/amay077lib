package com.amay077.lang;

import hu.akarnokd.reactive4java.base.Option;

public class OptionObservableValue<T> extends ObservableValue<Option<T>> {
	public OptionObservableValue() {
		super(Option.<T>none());
	}
}
