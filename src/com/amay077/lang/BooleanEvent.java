package com.amay077.lang;

import hu.akarnokd.reactive4java.base.Func2;

import java.util.ArrayList;
import java.util.List;

import com.amay077.android.collections.Lambda;

import android.view.View;

public class BooleanEvent<TView extends View, TData> {
	public interface EventHandler<TView, TData> {
		boolean handle(TView sender, TData data);
	}
	
	private final List<EventHandler<TView, TData>> _handlers = new ArrayList<EventHandler<TView, TData>>();
	
	public void add(EventHandler<TView, TData> handler) {
		_handlers.add(handler);
	}

	public void remove(EventHandler<TView, TData> handler) {
		_handlers.remove(handler);
	}
	
	public void clear() {
		_handlers.clear();
	}

	public boolean fire(TView sender, TData data) {
		final List<Boolean> returns = new ArrayList<Boolean>();
		for (EventHandler<TView, TData> h : _handlers) {
			returns.add(h.handle(sender, data));
		}
		
		return Lambda.fold(returns, new Func2<Boolean, Boolean, Boolean>() {
			@Override
			public Boolean invoke(Boolean a, Boolean b) {
				return a || b;
			}
		}, false);
	}
}
