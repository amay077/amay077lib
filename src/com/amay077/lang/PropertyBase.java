package com.amay077.lang;

public abstract class PropertyBase<T> extends Property<T> {
	protected abstract void internalSet(T value);
	
	@Override
	public final void set(T value) {
		T oldValue = get();
		if (!ObjectUtil.equals(oldValue, value)) {
			internalSet(value);
			fireOnChanged(oldValue, value);
		}
	};
	
	@Override
	public void _setWithoutFire(T value) {
		T oldValue = get();
		if (!ObjectUtil.equals(oldValue, value)) {
			internalSet(value);
			_fireOnChanged(oldValue, value);
		}
	}

}
