package com.amay077.reactive.geolocation.google;

import hu.akarnokd.reactive4java.base.Func1;
import hu.akarnokd.reactive4java.reactive.Observable;
import hu.akarnokd.reactive4java.reactive.Observer;
import hu.akarnokd.reactive4java.reactive.Reactive;

import java.io.Closeable;
import java.io.IOException;
import java.security.InvalidParameterException;

import com.amay077.android.util.Log;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

public class GoogleFunc {
	private static final String TAG = "GoogleFunc";

	private GoogleFunc() {
	}
	
	/**
	 * 位置を取得し続ける(finish は呼ばれない)
	 */
	public static Observable<Location> getCurrentLocationAsObservable(			
			final Context context, final String provider) {
		Log.d(TAG, "getCurrentLocationAsObservable() called.");
		return Reactive.createWithCloseable(new Func1<Observer<? super Location>, Closeable>() {
			private volatile boolean stop = false;

			@Override
			public Closeable invoke(final Observer<? super Location> observer) {
				
				final LocationManager locMan = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
				final LocationListener listener = new LocationListener() {
					
					@Override
					public void onStatusChanged(String provider, int status, Bundle extras) {
						Log.d(TAG, "getCurrentLocationAsObservable onStatusChanged() status - " + status);
					}
					
					@Override
					public void onProviderEnabled(String provider) {
						Log.d(TAG, "getCurrentLocationAsObservable onProviderEnabled() provider - " + provider);
					}
					
					@Override
					public void onProviderDisabled(String provider) {
						Log.d(TAG, "getCurrentLocationAsObservable onProviderDisabled() provider - " + provider);
						observer.error(new InvalidParameterException("LocationProvider disabled."));
					}
					
					@Override
					public void onLocationChanged(Location location) {
						Log.d(TAG, "getCurrentLocationAsObservable onLocationChanged() location - " + location);
						if (stop) {
							Log.d(TAG, "getCurrentLocationAsObservable onLocationChanged() stopped.");
							return;
						}
						
						observer.next(location);
					}
				};
				
				try {
					Log.d(TAG, "getCurrentLocationAsObservable requestLocationUpdates() called.");
					locMan.requestLocationUpdates(provider, 0, 0, listener, Looper.getMainLooper());
				} catch (Exception e) {
					observer.error(e);
				}

				return new Closeable() {
					@Override
					public void close() throws IOException {
						Log.d(TAG, "getCurrentLocationAsObservable close() called.");
						if (stop) {
							Log.i(TAG, "getCurrentLocationAsObservable close() already closed.");
							return;
						}
						stop = true;
						locMan.removeUpdates(listener);
						observer.finish();
					}
				};
			}
		});
	}

	/**
	 * 位置を取得し続ける(finish は呼ばれない)
	 */
	public static Observable<Location> getLatestLocationAsObservable(			
			final Context context) {
		Log.d(TAG, "getLatestLocationAsObservable() called.");
		return Reactive.createWithCloseable(new Func1<Observer<? super Location>, Closeable>() {
			private volatile boolean stop = false;

			@Override
			public Closeable invoke(final Observer<? super Location> observer) {
				
				final LocationManager locMan = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
				try {
					Log.d(TAG, "getLatestLocationAsObservable requestLocationUpdates() called.");
					
					Location gpsPos = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					Location netPos = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					
					if (gpsPos != null && netPos == null) {
						observer.next(gpsPos);
					} else if (gpsPos == null && netPos != null) {
						observer.next(netPos);
					} else if (gpsPos != null && netPos != null) {
						if (gpsPos.getAccuracy() < netPos.getAccuracy()) {
							observer.next(gpsPos);
						} else {
							observer.next(netPos);
						}
					}
					
					observer.finish();
				} catch (Exception e) {
					observer.error(e);
				}

				return new Closeable() {
					@Override
					public void close() throws IOException {
						Log.d(TAG, "getLatestLocationAsObservable close() called.");
						if (stop) {
							Log.i(TAG, "getLatestLocationAsObservable close() already closed.");
							return;
						}
						stop = true;
						observer.finish();
					}
				};
			}
		});
	}
}
