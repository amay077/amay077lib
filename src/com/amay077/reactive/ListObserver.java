package com.amay077.reactive;

import java.util.ArrayList;
import java.util.List;

import hu.akarnokd.reactive4java.reactive.Observer;

public class ListObserver<T> {
	public Observer<T> bind() {
		return new Observer<T>() {
			private List<T> _results = new ArrayList<T>();

			@Override
			public void next(T o) {
				_results.add(o);
			}

			@Override
			public void finish() {
				try {
					onFinish(_results);
				} finally {
					onPostExecute();
				}
			}

			@Override
			public void error(Throwable e) {
				try {
					onError(e);
				} finally {
					onPostExecute();
				}
			}
		};
	}
	
	public void onFinish(List<T> results) { };
	public void onError(Throwable err) { };
	public void onPostExecute() {};
}
