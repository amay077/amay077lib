package com.amay077.reactive;

import hu.akarnokd.reactive4java.reactive.Observer;

public class BasicObserver<T> {

	public Observer<T> bind() {
		return new Observer<T>() {

			@Override
			public void next(T o) {
				if (o != null) {
					onNext(o);
				}
			}

			@Override
			public void finish() {
				try {
					onFinish();
				} finally {
					onPostExecute();
				}
			}

			@Override
			public void error(Throwable e) {
				try {
					onError(e);
				} finally {
					onPostExecute();
				}
			}
		};
	}
	
	public void onNext(T o) { };
	public void onFinish() { };
	public void onError(Throwable err) { };
	public void onPostExecute() {};
}
