package com.amay077.reactive;

import android.os.Handler;

import com.amay077.android.util.Log;

import hu.akarnokd.reactive4java.reactive.Observer;

public class ReactiveUtil {
	private ReactiveUtil() {
	}
	
	public static <T> Observer<T> logcatObserver() {
		return logcatObserver("Rx", "");
	}
	
	public static <T> Observer<T> logcatObserver(final String tag, final String prefix) {
		return new Observer<T>() {

			@Override
			public void error(Throwable ex) {
				Log.d(tag, prefix + "-error:" + ex.toString());
			}

			@Override
			public void finish() {
				Log.d(tag, prefix + "-finish");
			}

			@Override
			public void next(T value) {
				Log.d(tag, prefix + "-next:" + value.toString());
			}
		};
	}
	
	static public <T> Observer<T> observerOnHandler(
			final Handler handler, final Observer<T> observer) {
		return new Observer<T>() {
			@Override
			public void next(final T value) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						observer.next(value);
					}
				});
			}

			@Override
			public void error(final Throwable ex) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						observer.error(ex);
					}
				});
			}

			@Override
			public void finish() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						observer.finish();
					}
				});
			}
		};
	}
}